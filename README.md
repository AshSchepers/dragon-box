## Dragon box
Dragon box is a Racket-based project utilising the DSL Rosette. The intention was to solve symbollic equations of the form
```
    4x - z - 5 + 5 - x + 7y - z - z + z - z = - 6 + y
```
Using the symbollics and constants of each side seperately, solutions for each symbol were attempted to be determined generically, so that the equation balanced out.

Rosette was determined to be inadequate for this use-case.
